﻿
function GoTo(s) {
    switch (s)
    {
        case "DGI":
            window.location = "http://www.digitalgateway.com";
            break;
    }
}


function TileHover() {
    $('.ezColorT1').hover(function () {
        $(this).addClass('ezColorT1-hover');
    }, function () {
        $(this).removeClass('ezColorT1-hover');
    });

    $('.ezColorT2').hover(function () {
        $(this).addClass('ezColorT2-hover');
    }, function () {
        $(this).removeClass('ezColorT2-hover');
    });

    $('.ezColorT3').hover(function () {
        $(this).addClass('ezColorT3-hover');
    }, function () {
        $(this).removeClass('ezColorT3-hover');
    });

    $('.ezColorT4').hover(function () {
        $(this).addClass('ezColorT4-hover');
    }, function () {
        $(this).removeClass('ezColorT4-hover');
    });

    $('.ezColorT5').hover(function () {
        $(this).addClass('ezColorT5-hover');
    }, function () {
        $(this).removeClass('ezColorT5-hover');
    });

    $('.ezColorT6').hover(function () {
        $(this).addClass('ezColorT6-hover');
    }, function () {
        $(this).removeClass('ezColorT6-hover');
    });

    $('.ezColorT7').hover(function () {
        $(this).addClass('ezColorT7-hover');
    }, function () {
        $(this).removeClass('ezColorT7-hover');
    });

}

function PageStartupUI() {
}

function FormatValue(val) {
    if (val == 0 || val == '' || val == undefined)
        return '---';
    else
        return val;
}


function GoBackRefresh() {
    window.location.href = document.ref;
}


function emptyFunction() {
}

function ButtonBarUISetup() {
    //Button Bar Image Hover
    $(".buttonBar .btnBarItem a img, .buttonBar .btnBarItemRight a img").addClass("btnBarBackgroundPlain");
    $(".buttonBar .btnBarItem a img, .buttonBar .btnBarItemRight a img")
    .mouseover(function () {
        $(this).addClass("btnBarBackgroundHover");
        $(this).removeClass("btnBarBackgroundPlain");
    })
    .mouseout(function () {
        $(this).addClass("btnBarBackgroundPlain");
        $(this).removeClass("btnBarBackgroundHover");
    });


    //Button Bar Hover PINK
    $(".buttonBar .btnBarPink a img")
    .mouseover(function () {
        $(this).addClass("btnBarBackgroundPink");
        $(this).removeClass("btnBarBackgroundPlain");
    })
    .mouseout(function () {
        $(this).addClass("btnBarBackgroundPlain");
        $(this).removeClass("btnBarBackgroundPink");
    });


    //Button Bar Hover Green
    $(".buttonBar .btnBarGreen a img")
    .mouseover(function () {
        $(this).addClass("btnBarBackgroundGreen");
        $(this).removeClass("btnBarBackgroundPlain");
    })
    .mouseout(function () {
        $(this).addClass("btnBarBackgroundPlain");
        $(this).removeClass("btnBarBackgroundGreen");
    });


    //Button Bar Text Hover
    $(".btnBarItem a, .btnBarItemRight a")
    .mouseover(function () {
        $(this).addClass("btnBarTextShadow");
    })
    .mouseout(function () {
        $(this).removeClass("btnBarTextShadow");
    });
}

function MoreButtonUISetup() {
    $(".ddDots")
    .mouseover(function () {
        $(this).addClass("ui-state-hover");
        $(this).addClass("ddDots-hover");
    })
    .mouseout(function () {
        $(this).removeClass("ui-state-hover");
        $(this).removeClass("ddDots-hover");
    });
}

function SetUpRoundButtons() {
    $(".btnRound a img").addClass("btnBarBackgroundPlain");
    $(".btnRound a img")
    .mouseover(function () {
        $(this).addClass("btnBarBackgroundHover");
        $(this).removeClass("btnBarBackgroundPlain");
    })
    .mouseout(function () {
        $(this).addClass("btnBarBackgroundPlain");
        $(this).removeClass("btnBarBackgroundHover");
    });

    $(".btnRoundSmall a img").addClass("btnBarBackgroundPlainSmall");
    $(".btnRoundSmall a img")
    .mouseover(function () {
        $(this).addClass("btnBarBackgroundHoverSmall");
        $(this).removeClass("btnBarBackgroundPlainSmall");
    })
    .mouseout(function () {
        $(this).addClass("btnBarBackgroundPlainSmall");
        $(this).removeClass("btnBarBackgroundHoverSmall");
    });

    $(".btnBarPink a img")
    .mouseover(function () {
        $(this).addClass("btnBarBackgroundPinkSmall");
        $(this).removeClass("btnBarBackgroundPlainSmall");
    })
    .mouseout(function () {
        $(this).addClass("btnBarBackgroundPlainSmall");
        $(this).removeClass("btnBarBackgroundPinkSmall");
    });

    $(".btnBarGreen a img")
    .mouseover(function () {
        $(this).addClass("btnBarBackgroundGreenSmall");
        $(this).removeClass("btnBarBackgroundPlainSmall");
    })
    .mouseout(function () {
        $(this).addClass("btnBarBackgroundPlainSmall");
        $(this).removeClass("btnBarBackgroundGreenSmall");
    });



}


function GoBack() {
    window.history.back();
}

function Print() {
    window.print();
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};
function isValidZipCode(zipCode) {
    return validZip(zipCode);
};

function validZip(zip) {
    if (zip.match(/(^\d{5}$)|(^\d{5}-\d{4}$)/)) {
        return true;
    }
    zip = zip.toUpperCase();
    if (zip.match(/^[A-Z][0-9][A-Z][0-9][A-Z][0-9]$/)) {
        return true;
    }
    if (zip.match(/^[A-Z][0-9][A-Z].[0-9][A-Z][0-9]$/)) {
        return true;
    }
    return false;
}

function IsDateValid(value) {
    try {
        var MonthIndex = 0;
        var DayIndex = 1;
        var YearIndex = 2;

        value = value.replace(/-/g, "/").replace(/\./g, "/");

        var SplitValue = value.split("/");
        var OK = true;
        if (!(SplitValue[DayIndex].length == 1 || SplitValue[DayIndex].length == 2)) {
            OK = false;
        }
        if (OK && !(SplitValue[MonthIndex].length == 1 || SplitValue[MonthIndex].length == 2)) {
            OK = false;
        }
        if (OK && SplitValue[YearIndex].length != 4) {
            OK = false;
        }
        if (OK) {
            var Day = parseInt(SplitValue[DayIndex], 10);
            var Month = parseInt(SplitValue[MonthIndex], 10);
            var Year = parseInt(SplitValue[YearIndex], 10);

            if (OK == ((Year > 1900) && (Year < new Date().getFullYear()))) {
                if (OK == (Month <= 12 && Month > 0)) {
                    var LeapYear = (((Year % 4) == 0) && ((Year % 100) != 0) || ((Year % 400) == 0));

                    if (Month == 2) {
                        OK = LeapYear ? Day <= 29 : Day <= 28;
                    }
                    else {
                        if ((Month == 4) || (Month == 6) || (Month == 9) || (Month == 11)) {
                            OK = (Day > 0 && Day <= 30);
                        }
                        else {
                            OK = (Day > 0 && Day <= 31);
                        }
                    }
                }
            }
        }
        return OK;
    }
    catch (e) {
        return false;
    }
}

function HelpClick(securityMask) {
    var h = 600;
    var w = 800;
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    var url = "http://help.digitalgateway.com/e-info?v=8.0.0&f=" + securityMask + "&l=en-US";
    var title = "@ResStrings.HELP";

    window.open(url, title, 'toolbar=yes, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

//////function setupLabel() {
//////    if ($('.label_check input').length) {
//////        $('.label_check').each(function () {
//////            $(this).removeClass('c_on');
//////        });
//////        $('.label_check input:checked').each(function () {
//////            $(this).parent('label').addClass('c_on');
//////        });
//////    };
//////    if ($('.label_radio input').length) {
//////        $('.label_radio').each(function () {
//////            $(this).removeClass('r_on');
//////        });
//////        $('.label_radio input:checked').each(function () {
//////            $(this).parent('label').addClass('r_on');
//////        });
//////    };
//////};

function PhoneNumberIsValid(phone) {
    var regPhone = new RegExp("^(011|1){0,1}([- .]){0,1}(\\d{3}|\\(\\d{3}\\))([- .]){0,1}(\\d{3})([- .]){0,1}(\\d{4})( *(ext|x)\\d{1,5})*$");
    return regPhone.test(phone);
}


function doGetCaretPosition(ctrl) {
    var CaretPos = 0;
    // IE Support
    if (document.selection) {
        ctrl.focus();
        var Sel = document.selection.createRange();
        Sel.moveStart('character', -ctrl.value.length);
        CaretPos = Sel.text.length;
    }
        // Firefox support
    else if (ctrl.selectionStart || ctrl.selectionStart == '0')
        CaretPos = ctrl.selectionStart;
    return (CaretPos);
}


function setCaretPosition(ctrl, pos) {
    if (ctrl.setSelectionRange) {
        ctrl.focus();
        ctrl.setSelectionRange(pos, pos);
    }
    else if (ctrl.createTextRange) {
        var range = ctrl.createTextRange();
        range.collapse(true);
        range.moveEnd('character', pos);
        range.moveStart('character', pos);
        range.select();
    }
}

